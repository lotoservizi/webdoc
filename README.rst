Template
========

$project si occupa di documentare il mini farmewark in uso persso Loto Servizi.


Features
--------

- facile da utilizzare
- molti esempi nei progetti realizzati

Installation
------------

Installare $project eseguendo:

    composer require lotoservizi/webfunc

Contribute
----------

- Issue Tracker: github.com/$project/$project/issues
- Source Code: github.com/$project/$project

Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@google-groups.com

License
-------

The project is licensed under the BSD license.
